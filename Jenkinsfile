#!groovy
envVariablesUtil.validateEnvVariablesAreSet('BOOKS_KUBERNETES_CLOUD_NAME', 'UNITY_CI_CD_SCRIPTS_REPOSITORY_URL', 'CDN_BUCKET', 'CDN_URL', 'ApiServicePath')

def defaultUnityAppVersion = "2019.2.1f1"
// TODO: use 'master' branch - INF-188 Merge Unity-CI-CD-Scripts heading branches
def defaultUnityCICDScriptsVersion = "heads/CORE-24--Create-Jenkins-flow-for-building-the-asset-bundles"
def defaultCsharpCICDScriptsVersion = "heads/master-build_partial_book"

properties([parameters([
        string(name: 'unity_ci_and_cd_scripts_version', defaultValue: defaultUnityCICDScriptsVersion, description: "Unity CI&CD Scripts version, tag from \"Unity-CI-CD-Scripts\" repository"),
        string(name: 'csharp_ci_and_cd_scripts_version', defaultValue: defaultCsharpCICDScriptsVersion, description: "CSharp CI&CD Scripts version, tag from \"Unity-CI-CD-Scripts\" repository"),
        booleanParam(name: 'build_ios', defaultValue: true, description: "Build iOS platform"),
        booleanParam(name: 'build_android', defaultValue: true, description: "Build Android platform"),
        booleanParam(name: 'use_pv', defaultValue: true, description: "Use persistence volume caching"),
        string(name: 'unity_app_version', defaultValue: defaultUnityAppVersion, description: "Unity App Version"),
        string(name: 'logLevel', defaultValue: "info", description: "Logs level"),
])])


def buildAssetBundlesExecutesMethod = "CICD.AssetBundlesBuilder.CommandLineBuild"
def packageBuilderExecutesMethod = "CICD.PackageBuilder.CommandLineBuild"
def featureConfigApiUpdaterExecutesMethod = "CICD.FeatureConfigApiUpdater.CommandLineBuild"
def unityAppVersion = params.unity_app_version
def UnityCICDScriptsVersion = params.unity_ci_and_cd_scripts_version
def platformsToBuild = unityUtil.getPlatformsToBuild();
def cloudName = env.BOOKS_KUBERNETES_CLOUD_NAME
def buildId = "${BUILD_NUMBER}_${new Date().getTime()}"
def gsRelativePath = "${env.CDN_BUCKET}/localization/${buildId}"

def createPodYaml(persistentVolumeClaimName, UnityAppVersion, platform) {
    def podProperties = unityUtil.defaultPodProperties(UnityAppVersion, platform)
    podProperties.persistentVolumeClaimName = persistentVolumeClaimName
    return unityUtil.createPodYaml(podProperties)
}

// define the slack notify build function
def notifyBuild(def message, def buildStatus) {

    // set default of build status
    buildStatus =  buildStatus ?: 'STARTED'
    def colorMap = [ 'STARTED': '#F0FFFF', 'SUCCESS': '#008B00', 'FAILURE': '#FF0000' ]
    def colorName = colorMap[buildStatus]

    withCredentials([string(credentialsId: 'Slack-BookfulCustomization', variable: 'slackCredentials')]) {
        slackSend (channel: '#bookful-customization', message: message, color: colorName, failOnError: true, teamDomain: 'ci-inceptionxr', token: slackCredentials)
    }
}

for (platform in platformsToBuild) {
    def label = k8s.normalizeId(
            "ja-unity-${platform}-${JOB_NAME}"
                    .toLowerCase()
                    .replace("scan repo", ""))

    k8s.createPersistentVolumeClaim(cloudName, label, "1Gi")
    def yaml = createPodYaml(label, unityAppVersion, platform)
    podTemplate(cloud: cloudName, label: label, showRawYaml: log.isDebug(), yaml: yaml, containers: [mono.k8sContainerTemplate()]) {
        node(label) {

            def projectPath = "${WORKSPACE}/.jenkins/Bookful-Localization"
            def unityArgs = ""
            unityArgs += " -projectPath ${projectPath}"
            unityArgs += " -buildTarget ${platform}"
            unityArgs += " -buildId=${buildId}"

            stage('checkout scm') {
                vcs.checkoutRepo(10)
                vcs.checkoutCustomRepo("${projectPath}/Assets/Plugins/Unity-CI-CD-Scripts", env.UNITY_CI_CD_SCRIPTS_REPOSITORY_URL, UnityCICDScriptsVersion)
            }

            stage("init \"${platform}\"") {
                def unityAppLicenceVersion = "${unityAppVersion}_${platform.toLowerCase()}"
                def unityLicenseFileCredentialsId = "UNITY_LICENSE_FILE_${unityAppLicenceVersion.replace(".", "_")}"
                unityUtil.activateLicence(unityLicenseFileCredentialsId)

                unityUtil.exeInContainer("mkdir -p .jenkins/Bookful-Localization/Assets/SourceResources")
                unityUtil.exeInContainer("cp -r content/* .jenkins/Bookful-Localization/Assets/SourceResources")

                if (params.use_pv) {
                    unityUtil.exeInContainer("cp -rp /pv_cache/* ${projectPath}")
                }

                string command = "-quit -batchmode"
                command += unityArgs
                unityUtil.exe(command, "Init")

                if (params.use_pv) {
                    unityUtil.exeInContainer("rm -rf /pv_cache/*")
                    unityUtil.exeInContainer("cp -rp ${projectPath}/Library /pv_cache")
                }
            }

            stage("\"${platform}\" asset bundles package") {
                string command = "-quit -batchmode"
                command += " -executeMethod ${buildAssetBundlesExecutesMethod} -outputPath=${projectPath}/AssetBundles/${platform}"
                command += unityArgs
                unityUtil.exe(command, "AssetBundles")
                if (params.use_pv) {
                    unityUtil.exeInContainer("rm -rf /pv_cache/*")
                    unityUtil.exeInContainer("cp -rp ${projectPath}/Library /pv_cache")
                }
            }

            stage("\"${platform}\" building package") {
                string command = "-quit -batchmode"
                command += " -executeMethod ${packageBuilderExecutesMethod}"
                command += unityArgs
                unityUtil.exe(command, "Building package")
            }

            stage("\"${platform}\" uploading content") {
                dir(projectPath) {
                    googleStorageUpload bucket: "gs://${gsRelativePath}", credentialsId: 'CDN_BUCKET', pattern: "Build/${platform}/**/*", pathPrefix: "Build"
                }
            }

            stage("\"${platform}\" updating API service") {
                dir(projectPath) {
                    unityCICDScriptsUtil.checkout()
                    unityCICDScriptsUtil.compileProject()

                    withCredentials([usernamePassword(credentialsId: 'bookfulApiServiceCredentials', usernameVariable: 'bookfulApiServiceUserName', passwordVariable: 'bookfulApiServicePassword')]) {
                        string command = unityArgs
                        command += " -executeMethod=${featureConfigApiUpdaterExecutesMethod}"
                        command += " -bookfulApiServiceUserName=${bookfulApiServiceUserName}"
                        command += " -bookfulApiServicePassword=${bookfulApiServicePassword}"
                        command += " -httpRelativePath=https://storage.googleapis.com/${gsRelativePath}/${platform}/"
                        command += " -CDNRelativePath=${env.CDN_URL}"
                        command += " -StorageBucketPath=https://storage.googleapis.com/${env.CDN_BUCKET}/"
                        command += " -ApiServicePath=${env.ApiServicePath}"

                        unityCICDScriptsUtil.exeInProject(command)
                    }
                }
            }

            def subject = "Job: ${platform} - #${env.BUILD_NUMBER} Finished"
            def message = "${subject} (${env.BUILD_URL})"
            notifyBuild(message, "SUCCESS")
        }
    }
}