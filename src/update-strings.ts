import { authorize } from './utils/gAuthUtil';
import { getSheetRange } from './utils/gSheetsUtil';
import { serializeObjectToFile } from './utils/fileUtil';
import { LanguageCode, getLocalizationIndexes } from './utils/localizationUtil';
import { doAndWaitForAll, getSettledResult, getSettledRejection } from './utils/asyncUtil';

const { spreadsheet: sheetConfig } = require('../config/settings.json');

const getExportPath = (domain: string, langCode: LanguageCode) =>
  `./content/${domain}/${langCode}/${domain.toLowerCase()}_localization_strings.json`;

type LocalizationStrings = {
  [localizationKey: string]: string;
};

type LocalizationMap = Map<LanguageCode, LocalizationStrings>;

type AllLocalizations = {
  client: LocalizationMap | undefined;
  cms: LocalizationMap | undefined;
};

const getLocalizationMaps = (sheetRange: string[][]): LocalizationMap | undefined => {
  if (!sheetRange) return undefined;

  const headerRow = sheetRange[0];
  const LocalizationIndexes = getLocalizationIndexes(headerRow);
  console.log(`Found ${sheetRange.length} rows for languages: ${[...LocalizationIndexes.keys()]}`);

  // TODO: can simplify this a bit?
  const dataRows: string[][] = sheetRange.slice(1);
  const localizationMap = dataRows.reduce(
    (acc: Map<LanguageCode, LocalizationStrings>, curRow: string[]) => {
      for (const [langCode, langColumnNumber] of LocalizationIndexes.entries()) {
        const localizationKey = curRow[0];
        const localizationValue = curRow[langColumnNumber];
        if (!localizationValue || !localizationKey) continue;

        if (!acc.has(langCode)) {
          acc.set(langCode, {});
        }

        let localizationMap = acc.get(langCode)!;

        if (localizationMap[localizationKey] != undefined)
          console.warn(`Setting duplicate key - ${localizationKey}`);

        localizationMap[localizationKey] = localizationValue;
        acc.set(langCode, localizationMap);
      }

      return acc;
    },
    new Map<LanguageCode, LocalizationStrings>()
  );

  return localizationMap;
};

function createLocalizationManifest(domain: string, localizationMap: LocalizationMap) {
  for (const [langCode, locStrings] of localizationMap) {
    console.log(`[${domain}] Localization language found: ${langCode}`);

    const exportPath = getExportPath(domain, langCode);
    serializeObjectToFile(exportPath, locStrings);
  }
}

function fetchLocaliztaions(auth, sheetTabName: string): Promise<LocalizationMap | undefined> {
  return getSheetRange(auth, sheetConfig.id, sheetTabName).then(range =>
    getLocalizationMaps(range)
  );
}

async function getAllLocalizations(auth): Promise<AllLocalizations> {
  const settledPromises = await doAndWaitForAll(
    fetchLocaliztaions,
    [auth, sheetConfig.clientTabName],
    [auth, sheetConfig.cmsTabName]
  );

  const [clientLocalizationMap, cmsLocalizationMap] = settledPromises.map(getSettledResult);
  const [clientError, cmsError] = settledPromises.map(getSettledRejection);
  if (clientError) {
    console.error(`Failed to parse client localization map! (error: ${clientError})`);
  }
  if (cmsError) {
    console.error(`Failed to parse cms localization map! (error: ${cmsError})`);
  }

  return {
    client: clientLocalizationMap,
    cms: cmsLocalizationMap
  };
}

//// Main logic
(async () => {
  const auth = authorize();
  if (!auth) return;

  const localizations = await getAllLocalizations(auth);

  if (localizations.client) createLocalizationManifest('Client', localizations.client);
  if (localizations.cms) createLocalizationManifest('CMS', localizations.cms);
})();
