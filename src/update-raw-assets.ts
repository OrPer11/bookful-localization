import { join as pathJoin } from 'path';
import { drive_v3 } from 'googleapis';
import { downloadFile, listFolderItems, getDrive, listSubfolderItems } from './utils/gDriveUtil';
import pLimit from 'p-limit';
import { groupByStatus } from './utils/asyncUtil';
import { parseLangCode, LanguageCode } from './utils/localizationUtil';

const rootFolderId = '1qrMMg8SdBu3I3GTsh3JK3KggOmHzD7aF';
const exportFolderPath = (domain: string, langCode: LanguageCode) =>
  `./content/${domain}/${langCode}/assets`;
const limit = pLimit(5); // limit concurrent downloads, download could throw errors when tring to download more then 5

interface LocalizationDriveFiles {
  langCode: LanguageCode;
  clientFiles: drive_v3.Schema$File[];
  cmsFiles: drive_v3.Schema$File[];
}

async function getLanguageFolderIds(
  rootFolderId: string,
  drive: drive_v3.Drive
): Promise<Map<LanguageCode, string> | null> {
  try {
    const folders = await listFolderItems(rootFolderId, drive);
    if (folders?.length === 0) return null;

    const map = new Map<LanguageCode, string>();
    for (const folder of folders!) {
      const langCode = parseLangCode(folder?.name);
      if (!langCode) continue;

      if (map.has(langCode))
        console.error(
          `Duplicate LanguageCode folder for (${langCode}). Replacing! Folder: ${folder.name}`
        );

      if (langCode && folder.id) {
        map.set(langCode, folder.id);
      }
    }

    return map;
  } catch (err) {
    console.error(err);
    return null;
  }
}

// Removes double extensions e.g. filename.mp3.mp3
function fixFileName(fileName: string | null | undefined): string | null {
  if (!fileName) return null;

  const splitted = fileName.split('.');
  const sLength = splitted.length;

  if (sLength > 2 && splitted[sLength - 1] === splitted[sLength - 2]) {
    splitted.pop();
    return splitted.join('.');
  }

  return fileName;
}

async function getLangFiles(
  rootFolderId: string,
  drive: drive_v3.Drive
): Promise<LocalizationDriveFiles[] | undefined> {
  const langToFolderIds = await getLanguageFolderIds(rootFolderId, drive);
  if (!langToFolderIds) {
    console.error('no localization folders were found');
    return;
  }

  const listFilesPromises: Promise<LocalizationDriveFiles>[] = [...langToFolderIds].map(
    ([langCode, folderId]) => {
      async function fnGetFolderIds() {
        const clientPromise = listSubfolderItems(folderId, 'Client', drive);
        const cmsPromise = listSubfolderItems(folderId, 'CMS', drive);

        const [clientFiles, cmsFiles] = await Promise.all([clientPromise, cmsPromise]);
        return { langCode, cmsFiles, clientFiles } as LocalizationDriveFiles;
      }

      return fnGetFolderIds();
    }
  );

  const settledPromises = await Promise.allSettled(listFilesPromises);
  return groupByStatus(settledPromises).fulfilledValues;
}

async function downloadDriveFiles(
  files: drive_v3.Schema$File[],
  exportFolder: string,
  drive: drive_v3.Drive
): Promise<Promise<string | undefined>[]> {
  return files.map(fileObj => {
    const fileName = fixFileName(fileObj.name);
    const filePath = pathJoin(exportFolder, fileName!);
    return limit(() => downloadFile(fileObj, filePath, drive));
  });
}

//// Main logic
(async () => {
  console.log('Accessing Google Drive API... ');
  const drive = getDrive();
  if (!drive) return;

  console.log('Getting language files...');
  const langFiles = await getLangFiles(rootFolderId, drive);
  if (!langFiles) return;

  const clientDlPromises = langFiles.flatMap(langFile => {
    const exportFolder = exportFolderPath('Client', langFile.langCode);
    return downloadDriveFiles(langFile.clientFiles, exportFolder, drive);
  });

  const cmsDlPromises = langFiles.flatMap(langFile => {
    const exportFolder = exportFolderPath('CMS', langFile.langCode);
    return downloadDriveFiles(langFile.cmsFiles, exportFolder, drive);
  });

  console.log('Downloading files...');
  const result = await Promise.allSettled(clientDlPromises.concat(cmsDlPromises));
  const rejected = groupByStatus(result).rejections.length;
  console.error(`Total file errors - ${rejected}`);
})();
