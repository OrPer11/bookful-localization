import { authorize } from './utils/gAuthUtil';
import { getSheetRange, parseSheetKVP } from './utils/gSheetsUtil';
import { serializeObjectToFile } from './utils/fileUtil';
import { doAndWaitForAll, getSettledResult, getSettledRejection } from './utils/asyncUtil';

const { spreadsheet: sheetConfig } = require('../config/settings.json');

type FontType = string;

interface TextCustomization {
  fontType: string;
}

interface TextCustomizationMap {
  [key: string]: TextCustomization;
}

type AllTextCustomMaps = {
  client: TextCustomizationMap | undefined;
  cms: TextCustomizationMap | undefined;
};

const getExportPath = (domain: string) => `./content/${domain}/${domain.toLowerCase()}_text_styles.json`;

function createFontManifest(domain: string, fontMap: TextCustomizationMap) {
  const exportPath = getExportPath(domain);
  console.log(`Writing fontMap to: ${exportPath}`);
  serializeObjectToFile(exportPath, fontMap);
}

function createCustomizationMap(textCustomizationMap: { [key: string]: FontType }): TextCustomizationMap {
  const keys = Object.keys(textCustomizationMap);
  return Object.fromEntries(
    keys.map(key => {
      return [key, { fontType: textCustomizationMap[key] }];
    })
  );
}

async function fetchFontMap(auth, sheetTabName): Promise<TextCustomizationMap | undefined> {
  const range: string[][] = await getSheetRange(auth, sheetConfig.id, sheetTabName);
  const fontTypes = parseSheetKVP(range, sheetConfig.fontTypeColumnName);

  if (!fontTypes) return undefined;
  return createCustomizationMap(fontTypes);
}

async function getAllTextCustomizationMaps(auth): Promise<AllTextCustomMaps> {
  const settledPromises = await doAndWaitForAll(
    fetchFontMap,
    [auth, sheetConfig.clientTabName],
    [auth, sheetConfig.cmsTabName]
  );


  const [clientLocalizationMap, cmsLocalizationMap] = settledPromises.map(getSettledResult);
  const [clientError, cmsError] = settledPromises.map(getSettledRejection);
  if (!clientLocalizationMap) {
    console.error(`Failed to parse client Text Customization map! Err: ${clientError}`);
  }
  if (!cmsLocalizationMap) {
    console.error(`Failed to parse cms TextCustomization map! Err: ${cmsError}`);
  }

  return {
    client: clientLocalizationMap,
    cms: cmsLocalizationMap
  };
}

// Main code
(async () => {
  const auth = authorize();
  if (!auth) return;

  const fontMaps = await getAllTextCustomizationMaps(auth);
  if (fontMaps.client) createFontManifest('Client', fontMaps.client);
  if (fontMaps.cms) createFontManifest('CMS', fontMaps.cms);
})();
