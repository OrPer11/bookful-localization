import { google, sheets_v4 } from 'googleapis';
import { GaxiosResponse } from 'gaxios';

export function getSheetRange(
  auth: any,
  spreadsheetId: string,
  tabName: string,
  tabRange: string = 'A:Z'
): Promise<string[][]> {
  const sheets = google.sheets({ version: 'v4', auth });
  const range = `${tabName}!${tabRange}`;

  return new Promise((resolve, reject) => {
    sheets.spreadsheets.values.get(
      {
        spreadsheetId,
        range
      },

      (err: Error | null, res: GaxiosResponse<sheets_v4.Schema$ValueRange>) => {
        if (err) {
          reject(`GoogleSheets API Error: ${err}`);
        } else if (!res?.data?.values?.length) {
          reject('No values found, or malformed data!');
        } else {
          resolve(res.data.values!);
        }
      }
    );
  });
}

/* Given a sheet range, create a mapping between two of it's columns (key:value)
 * Key column given by index (default first column)
 * Value column by name
 */
export function parseSheetKVP(
  sheetRange: string[][],
  valueColumnName,
  keyColumnIdx = 0
): { [key: string]: string } | null {
  const headerRow = sheetRange[0].map(cell => cell.toLowerCase());

  const fontTypesIdx = headerRow.indexOf(valueColumnName.toLowerCase());
  if (fontTypesIdx === -1) {
    console.error(
      `Trying to get key-values from sheet, but value column was not found! (name: "${valueColumnName}")`
    );
    return null;
  }

  const dataRows = sheetRange.slice(1);
  return dataRows.reduce((acc, row) => {
    const key = row[keyColumnIdx];
    if (!key) return acc; // Don't add row if empty string

    const fontType = row[fontTypesIdx];
    acc[key] = fontType;

    return acc;
  }, {});
}
