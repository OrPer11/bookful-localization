// Used to filter nulls/undefind from arrays - using arr.filter(hasValue)
export function hasValue<T>(value: T | null | undefined): value is T {
  return value != null && value != undefined;
}
