import { authorize } from './gAuthUtil';
import { google, drive_v3 } from 'googleapis';
import { createParentDirsSync } from './fileUtil';
import { createWriteStream } from 'fs';

export function getDrive(): drive_v3.Drive | null {
  const auth = authorize();
  if (!auth) return null;

  return google.drive({ version: 'v3', auth });
}

export async function listSubfolderItems(
  rootFolderId: string,
  subFolderPath: string,
  drive: drive_v3.Drive
): Promise<drive_v3.Schema$File[] | undefined> {
  const folders = subFolderPath.split('/');

  let currFolderId = rootFolderId;

  for (const folderName of folders) {
    const folderItems = await listFolderItems(currFolderId, drive);
    const nextFolder = folderItems?.find(folder => folder.name === folderName);
    if (!nextFolder) {
      console.error(`gDriveUtil: failed to find subFolder ${folderName}!`);
      return;
    }

    currFolderId = nextFolder.id!;
  }

  return await listFolderItems(currFolderId, drive);
}

export function listFolderItems(
  folderId: string | null | undefined,
  drive: drive_v3.Drive
): Promise<drive_v3.Schema$File[] | undefined> {
  return drive.files
    .list({
      q: `'${folderId}' in parents and trashed = false`,
      fields: 'files(id, name)'
    })
    .then(res => {
      const files = res?.data?.files;
      if (files?.length) {
        return files;
      } else {
        console.log('No items were found.');
      }
    });
}

export async function downloadFile(
  file: drive_v3.Schema$File,
  savePath: string,
  drive: any
): Promise<string | undefined> {
  if (!file?.id) return;
  const res = await drive.files.get(
    {
      fileId: file.id,
      alt: 'media'
    },
    {
      responseType: 'stream'
    }
  );

  createParentDirsSync(savePath);
  const dest = createWriteStream(savePath);

  return new Promise((resolve, reject) => {
    if (!res?.data) reject();
    res.data
      .on('end', () => {
        console.log(`Downloaded file - ${savePath}`);
        resolve(savePath);
      })
      .on('error', (err: any) => {
        console.error('Error downloading file.');
        reject(err);
      })
      .pipe(dest);
  });
}
