import { getDrive, listSubfolderItems } from '../gDriveUtil';

// TODO: Extract to proper unit tests
async function testGetDir(rootFolderId: string, subFolderPath: string, drive: drive_v3.Drive) {
  const resultFolder = await listSubfolderItems(rootFolderId, subFolderPath, drive);
  const resultFiles = resultFolder?.map(item => item.name).slice(0, 10);
  console.log(`testGetDir(${rootFolderId}, ${subFolderPath}) -> ${JSON.stringify(resultFiles)}`);
}

(async () => {
  const drive = getDrive()!;
  const testRootId = '1qrMMg8SdBu3I3GTsh3JK3KggOmHzD7aF';
  await testGetDir(testRootId, 'Spanish-ES', drive);
  await testGetDir(testRootId, 'Spanish-ES/CMS', drive);
  await testGetDir(testRootId, 'Spanish-ES/FAKE_FOLDER/CMS/', drive);
})();
