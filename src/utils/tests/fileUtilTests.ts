const fileUtil = require('../filesUtil');
const assert = require('assert').strict;

// TODO: Extract to proper unit tests
function testGetParentDirs(filePath: string, expectedResult: string[]) {
  try {
    const result = fileUtil.getParentDirs(filePath);
    assert.deepEqual(
      result,
      expectedResult,
      `GetParentDirs: Test failed! parsing ${filePath}, Expected: ${expectedResult}. Actual: ${result}`
    );
    return true;
  } catch {
    return false;
  }
}

function testSuite_getParentDirs() {
  const tests = [
    testGetParentDirs('/test/A/a.mp3', ['/', 'test', 'A']),
    testGetParentDirs('/test/Folder', ['/', 'test']),
    testGetParentDirs('/test1/test2/A/', ['/', 'test1', 'test2', 'A']), // TODO: is this correct behaviour?
    testGetParentDirs('./test/A/', ['.', 'test', 'A']), // TODO: is this correct behaviour?
    testGetParentDirs('C:\\test\\A\\', ['C:\\', 'test', 'A'])
  ];

  const successful = tests.filter(t => t).length;
  const failures = tests.length - successful;
  console.log(`GetParentDirs Tests: Success: ${successful}. Failed: ${failures}`);
}

testSuite_getParentDirs();
