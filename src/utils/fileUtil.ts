import fs from 'fs';
import { parse } from 'path';
import { customStringify } from './jsonUtil';

export function serializeMapToFile(exportFolder: string, obj: Map<string, any>) {
  const saveObj = mapToObject(obj);
  return serializeObjectToFile(exportFolder, saveObj);
}

export function serializeObjectToFile(exportFolder: string, obj: Object) {
  const json = customStringify(obj, undefined, 2);
  writeFileRecursiveSync(exportFolder, json, 'utf8');
}

export function writeFileRecursiveSync(filePath: string, data: any, charset: string) {
  // Note: if you pass a directory path here ('/test/dir/') it will not create the leaf dir
  createParentDirsSync(filePath);
  fs.writeFileSync(filePath, data, charset);
}

export function createParentDirsSync(filePath: string) {
  const dirPath = parse(filePath).dir;
  fs.mkdirSync(dirPath, { recursive: true });
}

// String manipulation only - Does not handle special path characters
export function getParentDirs(filePath: string): string[] {
  filePath += 'fix'; // Fix for paths of form '/end/with/slash/' ("slash" will be skipped if no filename)
  const initPath = parse(filePath);
  let remainingDirs = initPath.dir;
  let resultDirs: string[] = [];

  while (remainingDirs && remainingDirs != initPath.root) {
    const parsed = parse(remainingDirs);
    const dir = parsed.name;

    resultDirs.unshift(dir);
    remainingDirs = parsed.dir;
  }

  if (initPath.root) resultDirs.unshift(initPath.root);

  return resultDirs;
}

export function mapToObject(m: Map<string, any>) {
  let retVal: any = {};
  for (let [k, v] of m) {
    if (v instanceof Map) {
      retVal[k] = mapToObject(v);
    } else {
      retVal[k] = v;
    }
  }
  return retVal;
}
