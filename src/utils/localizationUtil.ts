export interface LanguageCode extends String {} // ISO 639-1 Code. (lowercase, 2-char)

export const getLocalizationIndexes = (arr: string[]): Map<LanguageCode, number> => {
  return arr.reduce((acc: Map<LanguageCode, number>, curValue: string, curIndex: number) => {
    const langCode = parseLangCode(curValue);
    if (langCode) {
      acc.set(langCode, curIndex);
    }
    return acc;
  }, new Map<LanguageCode, number>());
};

export const parseLangCode = (value: string | null | undefined): LanguageCode | null => {
  const rowLocalizationRegexp = /-(..)/i;
  const localCodeMatch = value?.match(rowLocalizationRegexp);
  if (!localCodeMatch) {
    console.log(`Local code wasn't match for: ${value}`);
    return null;
  }

  return localCodeMatch[1].toLowerCase();
};
