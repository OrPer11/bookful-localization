type JsonCompatible = Object | Array<JsonCompatible> | string | number;
type StringifyReplacer = (this: any, key: string, value: any) => any;

export function customStringify(
  obj: JsonCompatible,
  replacer?: StringifyReplacer,
  space?: string | number
): string {
  const origJson = JSON.stringify(obj, replacer, space);
  // Fix double escaped newlines (replace literals: \\n -> \n)
  return origJson.replace(/\\\\n/g, '\\n');
}
