export function groupByStatus<T>(settledPromises: PromiseSettledResult<T>[]) {
  const result = {
    fulfilledValues: new Array<T>(),
    rejections: new Array<any>()
  };

  for (const settled of settledPromises) {
    if (settled.status === 'fulfilled') {
      result.fulfilledValues.push(settled.value);
    } else if (settled.status == 'rejected') {
      result.rejections.push(settled.reason);
    } else throw 'Settled promise was not fulfilled or rejected!';
  }

  return result;
}

export function getSettledRejection<T>(settled: PromiseSettledResult<T>): string | undefined {
  if (settled.status !== 'rejected') return undefined;
  return settled.reason;
}

export function getSettledResult<T>(settled: PromiseSettledResult<T>): T | undefined {
  if (settled.status !== 'fulfilled') return undefined;
  return settled.value;
}

/* Call an async function multiple times, and return results*/
export async function doAndWaitForAll<T>(
  func: (...args: any[]) => Promise<T>,
  ...argsArrs: any[][] // expected- one params array per call to func
): Promise<PromiseSettledResult<T>[]> {
  const promises = argsArrs.map(args => func(...args));
  const settledPromises = await Promise.allSettled(promises);

  return settledPromises;
}
