using System.Collections.Generic;
using UnityEditor.Build;

namespace Editor.Interfaces
{
    public interface IBuildPreprocessor : IOrderedCallback
    {
        void OnPreprocessBuild(Dictionary<string, object> context);
    }
}