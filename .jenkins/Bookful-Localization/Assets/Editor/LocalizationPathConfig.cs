namespace Editor
{
    public static class LocalizationPathConfig
    {
        public const string RootFolder = "Assets/SourceResources";
        public const string ClientRootFolder = RootFolder +"/Client";
        public const string CMSRootFolder = RootFolder +"/CMS";
    }
}