using System.Collections.Generic;
using System.IO;
using System.Linq;
using Editor.Interfaces;
using UnityEditor;
using UnityEngine;
using Logger = CICD.Utils.Logger;

namespace Editor
{
    public class LocalizationRawFilesPackageBuilder : IAssetPackageBuildPreprocessor
    {
        private static readonly Logger Logger = new Logger(nameof(LocalizationRawFilesPackageBuilder));

        public int callbackOrder => 0;

        public void OnPreprocessBuild(Dictionary<string, object> context)
        {
            context.TryGetValue("BuildFolderPath", out var buildFolderPath);
            Process(LocalizationPathConfig.ClientRootFolder, (string) buildFolderPath);
        }

        private static void Process(string rootDirectoryPath, string outputDirectoryPath)
        {
            var files = Directory
                .GetFiles(rootDirectoryPath, "*.*", SearchOption.AllDirectories)
                .Where(IsRawFile)
                .ToList();

            Logger.Info($"Take raw files found {files.Count} files");

            foreach (var file in files)
            {
                var relativeFilePath = file
                    .Replace($"{rootDirectoryPath}/", "");
                var filePath = Path.Combine(outputDirectoryPath, relativeFilePath);
                new FileInfo(filePath).Directory.Create();

                File.Copy(file, filePath);
            }
        }

        private static bool IsRawFile(string filePath)
        {
            return filePath.EndsWith(".json");
        }

        #region MenuItems

        [MenuItem("Localization/Raw Assets/Build")]
        public static void DefineAssetBundles()
        {
            Process(LocalizationPathConfig.ClientRootFolder , "Build/");
        }

        #endregion
    }
}