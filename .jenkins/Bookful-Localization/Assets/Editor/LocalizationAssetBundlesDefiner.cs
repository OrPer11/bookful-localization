using System;
using System.Collections.Generic;
using System.Linq;
using CICD.Utils;
using Editor.Interfaces;
using UnityEditor;

namespace Editor
{
    public class LocalizationAssetBundlesDefiner : IAssetBundlesBuildPreprocessor
    {
        private static readonly Logger Logger = new Logger(nameof(LocalizationAssetBundlesDefiner));
        public int callbackOrder => 0;

        public void OnPreprocessBuild(Dictionary<string, object> context)
        {
            Process(LocalizationPathConfig.ClientRootFolder);
        }

        private static void Process(string folder)
        {
            Logger.Info($"LocalizationAssetBundles: Processing folder: {folder}");
            var assetBundleBuildIdentifier = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

            var localeFolders = AssetDatabase.GetSubFolders(folder);
            foreach (var localeFolder in localeFolders)
            {
                var locale = localeFolder.Split('/').Last();
                SetLocaleAssetBundle(locale, localeFolder, assetBundleBuildIdentifier);
            }
        }

        private static void SetLocaleAssetBundle(string locale, string assetRootPath, long identifier)
        {
            var assetPaths = AssetDatabase.FindAssets("*", new[] {$"{assetRootPath}/assets"})
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(assetPath =>
                {
                    SetAssetBundle(assetPath, $"{locale}/{locale}", identifier);
                    return assetPath;
                });

            Logger.Info($"{nameof(SetLocaleAssetBundle)}: added {assetPaths.Count()} files to '{locale}' asset bundle");
        }

        private static void SetAssetBundle(string assetPath, string assetBundleName, long identifier)
        {
            if (assetPath == null)
            {
                return;
            }

            AssetImporter.GetAtPath(assetPath).SetAssetBundleNameAndVariant($"{assetBundleName}_{identifier}", null);
        }

        #region MenuItems

        [MenuItem("Localization/Asset Bundles/Define")]
        public static void DefineAssetBundles()
        {
            Process(LocalizationPathConfig.ClientRootFolder);
        }

        #endregion
    }
}